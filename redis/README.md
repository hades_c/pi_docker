使用命令
docker run --restart=always -it --name redis_server -v /etc/localtime:/etc/localtime -p 6379:6379 -v /home/pi/data/redis/data:/data --privileged -d hypriot/rpi-redis redis-server
