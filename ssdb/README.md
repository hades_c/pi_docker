使用命令
docker build -t chiron/ssdb-arm .

docker run --restart=always -it --name ssdb-server -p 8888:8888 -v /home/pi/data/ssdb/data:/var/lib/ssdb --privileged -d chiron/ssdb-arm ssdb-server
